# Generated by Django 2.2.2 on 2019-07-31 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=45)),
                ('descricao', models.TextField()),
                ('codigo', models.CharField(max_length=20)),
                ('imagem', models.ImageField(null=True, upload_to='')),
                ('quantidade', models.PositiveIntegerField()),
            ],
        ),
    ]
