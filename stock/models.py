from django.db import models

# Create your models here.

class Product(models.Model):
	nome = models.CharField(max_length=45)
	descricao = models.TextField()
	codigo = models.CharField(max_length=20)
	imagem = models.ImageField(null=True)
	quantidade = models.PositiveIntegerField()

	def __str__(self):
		return self.codigo + ' - ' + self.nome

